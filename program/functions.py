import cv2, cv, os, re, tesseract, unicodedata
from bs4 import BeautifulSoup
import numpy as np
from operator import itemgetter
from text_recognition import text_recogntion_class

class function_class:
    def extract_text_details(self,current_path, output_path,text_data_file,image_path,only_image_name):
        #--store text in text_data[]
        text_data = []
        # --create txt file to store text data
        image = cv.LoadImage(image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)

        #--identify text with tesseract
        api = tesseract.TessBaseAPI()
        api.Init(".", "fra", tesseract.OEM_DEFAULT)
        tesseract.SetCvImage(image, api)
        textposition = api.GetHOCRText(1)
        #--get text and location from .xml file
        soup = BeautifulSoup(textposition, "lxml")
        for span in soup.findAll("span", {"class": "ocrx_word"}):
            # --check if hocr file has a string or not '<span...> </span> against <span>abc</span>
            if len(span) > 0:
                bbox_data = (span.get('title'))
                only_numbers = re.findall(r'\d+', bbox_data)
                x1, y1, x2, y2 = int(only_numbers[0]),int(only_numbers[1]),int(only_numbers[2]),int(only_numbers[3])
                centre_x = x1+abs(x1-x2)
                centre_y = y1 + abs(y1-y2)
                text_name = unicodedata.normalize('NFKD', span.string).encode('ascii', 'ignore')
                final_word = ''
                for character in text_name:
                    new_char = ''
                    # --replace 1 with 'i' since both are similar
                    if character == '1':
                        new_char = 'i'
                    else:
                        new_char = character
                    if new_char.isalpha() or new_char == '/':
                        # -- add character to word after converting to lower case
                        final_word = final_word + new_char.lower()
                #--write text to file
                text_data_file.write(final_word+' : ['+str(centre_x)+','+str(centre_y)+']'+'\n')
                text_data.append([final_word,[centre_x,centre_y]])
        if len(text_data)==0:
            print 'Error: No Text Detected in ', only_image_name

        return text_data

    def find_text_tesseract(self,current_path, image_path,text_data_file,only_image_name):
        text_cordinates = []
        dimension_image = cv2.imread(image_path,cv2.CV_LOAD_IMAGE_GRAYSCALE)
        image_height, image_width = dimension_image.shape
        image = cv.LoadImage(image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)

        api = tesseract.TessBaseAPI()
        api.Init(".","fra",tesseract.OEM_DEFAULT)
        # api.SetPageSegMode(tesseract.PSM_AUTO)
        tesseract.SetCvImage(image,api)
        tesseract_text=api.GetUTF8Text()
        textposition=api.GetHOCRText(1)



        tesseract_words = []
        soup = BeautifulSoup(textposition, "lxml")
        for span in soup.findAll("span", {"class": "ocrx_word"}):
            # no_text_image = self.my_function_remove_text(span,image)
            #--check if hocr file has a string or not '<span...> </span> against <span>abc</span>
            if len(span)>0:
                bbox_data = (span.get('title'))
                only_numbers =  re.findall(r'\d+',bbox_data)
                x1 = int(only_numbers[0])
                y1 = int(only_numbers[1])
                x2 = int(only_numbers[2])
                y2 = int(only_numbers[3])
                word_confidence = only_numbers[4]
                bounding_box = [x1,y1,x2,y2]
                x_difference,y_difference = abs(x1-x2),abs(y1-y2)
                if (x_difference- y_difference)>=0 and x_difference>20 and span.string!=None:

                    # cv2.rectangle(orginal_img, (x1, y1), (x2, y2), (255, 255, 255), -1)
                    #--remove symbols from text
                    text_name = unicodedata.normalize('NFKD', span.string).encode('ascii','ignore')
                    if len(text_name) == 0:
                        continue
                    #-- count number of occurences per character to delete 'OO' like false identification
                    #-- this also deletes single chracter words
                    exit_condition_1 = self.calculate_character_occurences(text_name)
                    if exit_condition_1:
                        continue

                    #---check each character and take them only if is letter or a '/'
                    final_word = ''
                    for character in text_name:
                        new_char = ''
                        # --replace 1 with 'i' since both are similar
                        if character == '1':
                            new_char = 'i'
                        else:
                            new_char = character
                        if new_char.isalpha() or new_char=='/':
                            #-- add character to word after converting to lower case
                            final_word = final_word+new_char.lower()
                    #--check if final word is more than one character
                    if len(final_word)>1:
                        tesseract_words.append([final_word,bounding_box,word_confidence])
                        continue


        # print '----------new array------------------'
        #----combine words too close to each other
        for w_row1, word_1 in enumerate(tesseract_words):
            before_x1,before_y1,before_x2,before_y2 = word_1[1]
            word_confidence1 = int(word_1[2])
            for w_row2,word_2 in enumerate(tesseract_words):
                if w_row2 > w_row1:
                    current_x1,current_y1,current_x2,current_y2 = word_2[1]
                    word_confidence2 = int(word_2[2])
                    #---horizontal word combination (distance, same lineish horizontally)
                    if abs(current_x1-before_x2)<35 and abs(current_y1-before_y1)<5:
                        new_word = word_1[0]+' '+word_2[0]
                        new_cordinates = before_x1,before_y1,current_x2,current_y2
                        new_word_confidence = (word_confidence1+word_confidence2)/2
                    #--vertical word combination (distance,same columnish vertically)
                    elif abs(current_y1-before_y2)<25 and abs(current_x1-before_x1)<65:
                        new_word = word_1[0]+' '+word_2[0]
                        new_cordinates = before_x1,before_y1,current_x2,current_y2
                        new_word_confidence = (word_confidence1+word_confidence2)/2
                    else:
                        new_word = ''
                        new_cordinates = ''
                        new_word_confidence = 0

                    if new_word != '':
                        #----check word orientation
                        x_difference,y_difference = abs(new_cordinates[0]-new_cordinates[2]),abs(new_cordinates[1]-new_cordinates[3])
                        if (x_difference- y_difference)>2 and x_difference>20:
                            tesseract_words[w_row1] = [new_word,new_cordinates,new_word_confidence]
                            del tesseract_words [w_row2]
                            break





        text_cordinates,tesseract_words = self.dictionary_matching(current_path, tesseract_words,text_cordinates,image_height)

        text_data = []
        for text_row in text_cordinates:
            final_word = text_row[0]
            centre_x, centre_y = text_row[1]
            # --write text to file
            text_data_file.write(final_word + ' : [' + str(centre_x) + ',' + str(centre_y) + ']' + '\n')
            text_data.append([final_word, [centre_x, centre_y]])


        return text_data

    def dictionary_matching(self,current_path, tesseract_words,text_cordinates,text_img_height):
        text_object = text_recogntion_class()

        #----use my own dictionary
        dictionary_content = ''
        with open(current_path+'/program/dict_french_POPLAR.txt') as f:
            dictionary_content = f.read().splitlines()

        finalized_words =[]
        for row_num,row in enumerate(tesseract_words):
            orginal_word = row[0]
            cordinates = row[1]
            confidence = row[2]

            #----check for slash and if exists seperate two words and check each with dictionary
            slash_seperated_words = []
            image_word = orginal_word
            slash_position = image_word.find('/')
            if slash_position != -1:
                slash_seperated_words.append(image_word[0:slash_position])
                slash_seperated_words.append(image_word[slash_position+1:])
                add_to_final_words = []
                for each_word in slash_seperated_words:
                    add_to_final_words,word_matched = self.find_matching_word(text_object,dictionary_content,each_word,add_to_final_words,orginal_word,cordinates,confidence)
                #---gather results from dictionary and combine them with slash
                recognized_word = ''
                edit_distance_value = -1
                for t,temp_row in enumerate(add_to_final_words):
                    if len(temp_row[4])==0:
                        recognized_word = ''
                        edit_distance_value = 0
                    else:
                        temp_word = temp_row[4]
                        temp_edit_distance = temp_row[5]
                        if t==0:
                            recognized_word = temp_word+'/'
                            edit_distance_value = temp_edit_distance
                        elif t==len(add_to_final_words)-1:
                            recognized_word = recognized_word+temp_word
                            edit_distance_value = edit_distance_value+temp_edit_distance
                        else:
                            recognized_word = recognized_word+temp_word+'/'
                            edit_distance_value = edit_distance_value+temp_edit_distance
                if len(recognized_word)>0:
                    finalized_words.append([orginal_word,cordinates,confidence,'D',recognized_word,edit_distance_value])
            #-----if no slash check word with dictionary
            else:
                #--first remove all spaces and check
                word_to_check = "".join(c for c in orginal_word if c not in (' '))
                finalized_words,word_matched = self.find_matching_word(text_object,dictionary_content,word_to_check,finalized_words,orginal_word,cordinates,confidence)



        #drawing rectangles around identified words (green)
        if len(text_cordinates)==0:
            iteration = 1
        else:
            iteration = 2
        for each_row in finalized_words:
            if len(each_row[4])==0:
                correct_word = each_row[0]
            else:
                correct_word = each_row[4]
            x1,y1,x2,y2 = text_object.hocr_text(each_row)
            x_centre = x1 + ((x2-x1)/2)
            y_centre = y1 + ((y2-y1)/2)
            if iteration==1:
                text_cordinates.append([correct_word,[x_centre,y_centre]])
            else:
                insert_x = y_centre
                insert_y = text_img_height-x_centre
                text_cordinates.append([correct_word,[insert_x,insert_y]])

        return text_cordinates,tesseract_words


    def calculate_character_occurences(self, text_name):
        count_letters = []
        for L in text_name:
            chracter_exists= False
            if len(count_letters)>0:
                for cur_row,C in enumerate(count_letters):
                    if C[0]==L:
                        chracter_exists = True
                        break
            if chracter_exists==False:
                count_letters.append([L,text_name.count(L)])
            else:
                continue
        exit_condition_1 = False

        #calculate if same letter is repeated or after removing '/' such, still only one letter is repeated
        alpha_character_count = 0
        for character_count in count_letters:
            if len(count_letters)==1:
                if character_count[1]==len(text_name):
                    exit_condition_1 = True
            else:
                if character_count[0].isalpha():
                    alpha_character_count+=1
        if alpha_character_count<2:
            exit_condition_1 = True

        return exit_condition_1

    def find_matching_word(self,text_object,dictionary_content,current_word,finalized_words,orginal_word,cordinates,confidence):
        word_exists, word_matched  = False, False
        suggestion,word_exists,existing_word = text_object.check_dictionary(dictionary_content,current_word,word_exists)
        if word_exists:
            finalized_words.append([orginal_word,cordinates,confidence,'org','',0])
            word_matched = True
            # print 'orginal_word,cordinates,',orginal_word,cordinates,''
        elif len(suggestion) is not 0:
            if len(current_word)>1:
                suggestion.sort(key=itemgetter(1))
                suggested_word = ''
                edit_distance_value = -1
                temp_list = []
                for suggest in suggestion:
                    if suggest[1]==1:
                        temp_list.append([suggest[0],suggest[1]])
                    else:
                        suggest_word = suggest[0]
                        if current_word[0]== suggest_word[0]:
                            temp_list.append([suggest_word,suggest[1]])
                if len(temp_list)>1:
                    original_word_length = len(current_word)
                    min_distance = 100
                    for temp_word in temp_list:
                        if original_word_length-len(temp_word[0])<min_distance:
                            suggested_word = temp_word[0]
                            edit_distance_value = temp_word[1]
                            min_distance = original_word_length-len(temp_word)
                else:
                    for temp_word in temp_list:
                        suggested_word = temp_word[0]
                        edit_distance_value = temp_word[1]
                if suggested_word != '':
                    finalized_words.append([current_word,cordinates,confidence,'dict',suggested_word,edit_distance_value])
                    word_matched = True
                    # print 'current_word,cordinates,suggested_word',current_word,cordinates,suggested_word

        return finalized_words,word_matched



    def extract_GT_AREA_contours(self,image_path,output_path,text_data,gt_contour_data_file):
        if '_A' in image_path:
            gt_image = cv2.imread(image_path, 0)
            #--find contours in gt_image
            ret, thresh = cv2.threshold(gt_image, 200, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            for count, cnt in enumerate(contours):
                for text_cord in text_data:
                    text = text_cord[0]
                    x,y = text_cord [1]
                    dist = cv2.pointPolygonTest(cnt, (x, y), False)
                    if dist == 1:
                        # img_height, img_width = gt_image.shape
                        # outer_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
                        # cv2.drawContours(outer_cont2, [cnt], 0, (0, 0, 0), 2, cv2.CV_AA)
                        # cv2.putText(outer_cont2, str(text), (x, y), cv2.FONT_HERSHEY_PLAIN, 5, (0, 0, 255), 5)
                        # cv2.imwrite(output_path + str(imagename[0:-7])+'_CONT' + str(count) + '.png', outer_cont2)

                        contour_data = ''
                        for c, cont_point in enumerate(cnt):
                            if c == 0:
                                contour_data = contour_data + '[' + str(cont_point[0][0]) + ' ' + str(
                                    cont_point[0][1]) + ']'
                            else:
                                contour_data = contour_data + ',[' + str(cont_point[0][0]) + ' ' + str(
                                    cont_point[0][1]) + ']'
                        gt_contour_data_file.write('contour_details : '+contour_data+' : ')
                        gt_contour_data_file.write(text+' : ['+str(x)+','+str(y)+']'+'\n')
                        break


    def extract_GT_LINE_contours(self,image_path,output_path,text_data,gt_contour_data_file):
        if '_L' in image_path:
            gt_image = cv2.imread(image_path, 0)
            #--find contours in gt_image
            ret, thresh = cv2.threshold(gt_image, 220, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            for count, cnt in enumerate(contours):
                contour_data = ''
                for c, cont_point in enumerate(cnt):
                    if c == 0:
                        contour_data = contour_data + '[' + str(cont_point[0][0]) + ' ' + str(
                            cont_point[0][1]) + ']'
                    else:
                        contour_data = contour_data + ',[' + str(cont_point[0][0]) + ' ' + str(
                            cont_point[0][1]) + ']'
                gt_contour_data_file.write('contour_details : '+contour_data + '\n')


