import os,traceback
from natsort import natsorted

from functions import function_class

class MainFormLogic:
    def __init__(self):
        current_path = os.path.abspath(os.path.join(os.getcwd(), '..'))
        function_obj = function_class()
        # --read each image file
        org_image_path = current_path + '/input/original_op/'
        input_image_list = natsorted(os.listdir(org_image_path))
        for img_num, imagename in enumerate(input_image_list):
            try:
                print imagename
                only_image_name = str(imagename[0:-4])
                # --create output folder
                output_path = current_path + '/output/'+only_image_name+'/'
                if not (os.path.exists(output_path)):
                    os.mkdir(output_path)

                #--text image path
                text_image_path = current_path + '/input/text/improved_' + str(imagename)
                #--create 'open_plan_text.txt' to store text data
                text_data_file = open(output_path + only_image_name + '_open_plan_text.txt', 'w')
                #--extract text data

                # text_data = function_obj.extract_text_details(current_path, output_path,text_data_file,text_image_path,only_image_name)
                text_data = function_obj.find_text_tesseract(current_path,text_image_path,text_data_file,only_image_name)

                # print '---', only_image_name,'----'
                # for row in text_data:
                #     print row

                # --read each GT file
                gt_image_path = current_path + '/input/GT/'
                input_image_list = os.listdir(gt_image_path)
                for gt_num,gt_file_name in enumerate(input_image_list):
                    # if img_num==0 and gt_num==1:
                    #--find GT filenames ==to original image name
                    if str(gt_file_name[0:-7])==only_image_name:
                        gt_name = str(gt_file_name[0:-4])
                        # --create txt file to store GT contour data
                        gt_contour_data_file = open(output_path + gt_name + '_GT_contour_data.txt', 'w')
                        image_path = gt_image_path + str(gt_file_name)
                        function_obj.extract_GT_AREA_contours(image_path,output_path,text_data,gt_contour_data_file)
                        function_obj.extract_GT_LINE_contours(image_path, output_path, text_data, gt_contour_data_file)
                        gt_contour_data_file.close()
                text_data_file.close()
            except:
                print '------Error in ',str(imagename),'------------------'
                traceback.print_exc()


if __name__ == '__main__':
        hwl1 = MainFormLogic()